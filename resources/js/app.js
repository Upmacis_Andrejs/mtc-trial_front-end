/*! Main script file of theme */
var $tablet_width = 991;
var $mobile_width = 767;
var $xs_width = 479;

$(document).ready(function() {
  'use strict';

  let $window = $(window);
  let $document = $(document);
  let $html = $("html");
  let $body = $("body");

  // Replace all .svg to .png, in case the browser does not support the format
  if(!Modernizr.svg) {
      $('img[src*="svg"]').attr('src', function() {
          return $(this).attr('src').replace('.svg', '.png');
      });
      $('*[style*="svg"]').attr('style', function() {
          return $(this).attr('style').replace('.svg', '.png');
      });
  }
  /**/

  // Add CSS class to Site Header when scrollTop position of the document is not 0
  var $lastY = $window.scrollTop();
  function add_not_top() {
    $body.addClass("not--top");
  }
  function remove_not_top() {
    $body.removeClass("not--top");
  }
  function add_not_top_down() {
    $body.addClass("not--top-down");
  }
  function remove_not_top_down() {
    $body.removeClass("not--top-down");
  }
  var $timeout_add_not_top;
  var $timeout_remove_not_top;
  var $timeout_add_not_top_down;
  var $timeout_remove_not_top_down;


  if( $lastY > 50 ) {
    add_not_top();
  }

  $window.scroll(function() {

    var $currentY = $window.scrollTop();
    if ( $currentY > $lastY ) {
      var y = 'down';
    } else if ( $currentY < $lastY ) {
      var y = 'up';
    }
    $lastY = $currentY;
    if ( $document.scrollTop() > 50 && y == 'down' ) {
      $timeout_add_not_top = setTimeout(add_not_top, 150);
    } else if ( $document.scrollTop() <= 80 && y == 'up' ) {
      $timeout_remove_not_top = setTimeout(remove_not_top, 150);
    }
    if ( $document.scrollTop() > 160 && y == 'down' ) {
      $timeout_add_not_top_down = setTimeout(add_not_top_down, 150);
    } else if  ( y == 'up' ) {
      $timeout_add_not_top_down = setTimeout(remove_not_top_down, 150);
    }

  });
  /**/

  // Image slider script
  function imageSlider() {
    if( $('.lightslider').length > 0 ) {
      let slider = 0;

      $(".lightslider").each(function() {
        slider += 1;
        let $this = $(this);
        let $this_slider = $('#lightslider-'+slider);

        $this.lightSlider({
          item: 3,
          auto: true,
          loop: true,
          pauseOnHover: true,
          speed: 1200,
          pause: 6000,
          onBeforeStart: function() {
            heroSliderFullWidth();
            if ( $this_slider.find('li').length < 3 ) {
              $this_slider.addClass('one-item');
            }
          },
          onSliderLoad: function() {
            heroSliderAdWidth();
            $this_slider.removeClass('cS-hidden').addClass('jsa-init');
          },
          onAfterSlide: function() {
            if( $('.hero-slider-wrap').find('.clone.right.active').length > 0 && !$('#hero-slider-css').length > 0 ) {
              $('head').append('<style id="hero-slider-css">.lightslider-item:not(.clone):after,.lightslider-item:not(.clone) .title-slider{transition:none!important;}</style>');
            } else if( !$('.hero-slider-wrap').find('.clone.right.active').length > 0 ) {
              $('#hero-slider-css').detach();
            }
          }
        });
      });

    }
  }
  imageSlider();
  /**/

  // Wrap button text in additional elements for correct positioning
  function btn() {
      $('.btn').each(function () {
          var $this = $(this);
          if (!$this.hasClass('jss-init')) {
              $this.wrapInner('<div class="btn-inner-cell"></div>');
              $this.wrapInner('<div class="btn-inner-table"></div>');
              $this.addClass('jsa-init');
          }
      });
  }
  btn();
  /**/

  // Match specific element heights within parent
  function matchHeights() {
    if ($('.js-equal-h-wrap-sm-up').length > 0) {
      //console.log('match heights');
      if ($window.width() > $xs_width) {
        $('.js-equal-h-wrap-sm-up').each(function () {
          var $equalHeightBlock = $(this).find('.js-equal-h');
          $equalHeightBlock.css('height', '');
          var $heights = $equalHeightBlock.map(function () {
              return $(this).outerHeight()
          }).get();
          var $maxHeight = parseFloat(Math.max.apply(null, $heights));
          $equalHeightBlock.css('height', $maxHeight);
        });
      } else {
        $('.js-equal-h-wrap-sm-up').each(function () {
          $(this).find('.js-equal-h').css('height', '');
        });
      }
    }
    if ($('.js-equal-h-wrap').length > 0) {
      $('.js-equal-h-wrap').each(function () {
        var $equalHeightBlock = $(this).find('.js-equal-h');
        $equalHeightBlock.css('height', '');
        var $heights = $equalHeightBlock.map(function () {
          return $(this).outerHeight()
        }).get();
        var $maxHeight = parseFloat(Math.max.apply(null, $heights));
        $equalHeightBlock.css('height', $maxHeight);
      });
    }
  }
  matchHeights();
  $window.resize(function () {
    matchHeights();
  });
  /**/

  // Open and close submenus
  $('.menu-item-has-children').on('mouseenter', function(e) {
      submenuWidth();
      matchHeights();
  });

  $('.menu-item-has-children>a').on('click', function(e) {
    if( $window.width() <= $tablet_width ) {
      e.preventDefault();
      submenuWidth();
      matchHeights();
      $(this).closest('li').toggleClass('jsa-submenu-open');
    }
  });
  $document.on('mouseup', function(e) {
    if ( $('.jsa-submenu-open').length > 0 ) {
      var container = $('.jsa-submenu-open');
      if (
          !container.is(e.target) // if the target of the click isn't the container...
          && container.has(e.target).length === 0 // ... nor a descendant of the container
        )
      {
        container.removeClass('jsa-submenu-open');
      }
    }
  });

  /**/

  // Calculate Hero slider full width
  function heroSliderFullWidth() {
    if( $('.hero-slider-full-width').length > 0 ) {
      //console.log('heroSLiderFullWidth()');
      var $this = $('.hero-slider-full-width');
      var $slideWidth = parseFloat($this.closest('.hero-slider-wrap').width());
      var $slideMargin = parseFloat($this.find('.lightslider-item').css('margin-right'));
      var $fullWidth = $slideWidth * 3 + $slideMargin * 2;
      $this.css('width', $fullWidth);
      $this.find('.lSPager, .lSAction').css('width', $slideWidth);
      //console.log('$fullWidth = ' + $fullWidth);
    }
  }
  $window.resize(function () {
    heroSliderFullWidth();
  });
  /**/

  // Set pager and action wrap width for hero slider
  function heroSliderAdWidth() {
    if( $('.hero-slider-full-width').length > 0 ) {
      var $this = $('.hero-slider-full-width');
      var $slideWidth = parseFloat($this.closest('.hero-slider-wrap').width());
      $this.find('.lSPager, .lSAction').css('width', $slideWidth);
    }
  }
  $window.resize(function () {
    heroSliderAdWidth();
  });
  /**/

  // Toggle mobile menu
  $('.mobile-menu-icon').click(function () {
      var $this = $(this);

      if ($this.hasClass('jsa-open')) {
          var $htmlTop = Math.abs(parseFloat($html.css('top'), 10));
          $html.css('top', '');
          $html.removeClass('mobile-menu-open');
          $this.removeClass('jsa-open');
          $window.scrollTop($htmlTop);
          $('.mobile-menu-wrap').stop(true, true).slideUp(250).scrollTop(0);
      } else {
          var $scrollTop = $window.scrollTop();
          $html.addClass('mobile-menu-open');
          $this.addClass('jsa-open');
          $('.mobile-menu-wrap').stop(true, true).slideDown(250);
          $html.css('top', -$scrollTop);
          submenuWidth();
      }
      return false;
  });

  $window.resize(function () {
      if ($window.width() >= $tablet_width) {
          $('.mobile-menu-icon').removeClass('jsa-open');
          $html.removeClass('mobile-menu-open');
          $('.mobile-menu-wrap').css('display', '');
          var $htmlTop = Math.abs(parseFloat($html.css('top'), 10));
          $window.scrollTop($htmlTop);
      }
  });
  /**/

  // Script for mobile header
  function mobileHeader() {
    //console.log('mobile header');
    var $siteHeader = $('.site-header');
    var $search = $('.search-form');
    var $logo = $('.site-logo');
    var $mainMenu = $('.main-menu');
    var $enquiriesEmail = $siteHeader.find('.enquiries-email');
    var $headerLinks = $siteHeader.find('.header-links');
    var $socialLinks = $siteHeader.find('.social-links');

    if( $window.width() <= $tablet_width && !$('.site-header .top-block .search-form').length > 0 ) {
      //console.log('to mobile header');
      var $mmAdditionalWrap = $('.mm-additional-wrap');

      $search.appendTo($('.top-block-search-wrap'));
      $logo.appendTo($('.top-block-logo-wrap'));
      $mainMenu.appendTo($('.mm-main-menu-wrap'));
      $enquiriesEmail.appendTo($mmAdditionalWrap);
      $headerLinks.appendTo($mmAdditionalWrap);
      $socialLinks.appendTo($mmAdditionalWrap);

    } else if($window.width() > $tablet_width && $('.site-header .top-block .search-form').length > 0  ) {
      //console.log('to desktop header');
      var $AdditionalWrap = $('.top-block .additional-wrap');

      $search.appendTo($('.main-menu-block .search-wrap'));
      $logo.appendTo($('.logo-block .site-logo-wrap'));
      $mainMenu.appendTo($('.main-menu-block .main-menu-wrap'));
      $enquiriesEmail.appendTo($('.top-block .enquiries-wrap'));
      $headerLinks.appendTo($AdditionalWrap);
      $socialLinks.appendTo($AdditionalWrap);

    }
  }
  mobileHeader();
  $window.resize(function () {
    mobileHeader();
  });
  /**/

  // Add .active to search-wrap
  $body.on('click', '.site-header .search-form .search-submit', function(e) {
    //console.log('clicked on submit');
    if($window.width() <= $tablet_width) {
      e.preventDefault();
      $body.toggleClass('jsa-searchform-active');
      if( $('.jsa-searchform-active').length > 0 ) {
        $('.site-header .search-form .search-input').focus();
      }
    }
  });
  $document.on('mouseup', function(e) {
    if ( $body.hasClass('jsa-searchform-active') ) {
      var container = $('.top-block-search-wrap');
      if (
          !container.is(e.target) // if the target of the click isn't the container...
          && container.has(e.target).length === 0 // ... nor a descendant of the container
        )
      {
        $body.removeClass('jsa-searchform-active');
      }
    }
  });
  /**/

  // Submit searchform on enter
  $('.search-form .search-input').keydown(function(e) {
    var key = e.which;
    if (key == 13) {
      $(this).closest('form').submit();
    }
  });
  /**/

  // Set submenu list wrap width
  function submenuWidth() {
    if( $('.submenu-wrap').length > 0 ) {
      //console.log('submenuWidth()');
      var $windowWidth = $window.width();
      var $fullWidth = parseFloat($('.submenu-wrap>.row').innerWidth());

      if( $windowWidth > $tablet_width ) {
        setTimeout(function() {
          $('.submenu-list-wrap').css('width', $fullWidth * .25);
        }, 10);
      } else if( $windowWidth <= $tablet_width && $windowWidth > $mobile_width ) {
        setTimeout(function() {
          $('.submenu-list-wrap').css('width', $fullWidth * .33);
        }, 10)
      } else if( $windowWidth <= $mobile_width && $windowWidth > $xs_width ) {
        setTimeout(function() {
          $('.submenu-list-wrap').css('width', $fullWidth * .5);
        }, 10);
      } else if ( $windowWidth <= $xs_width ) {
        $('.submenu-list-wrap').css('width', $fullWidth * 1);
      }
    }
  }
  submenuWidth();
  $window.resize(function () {
    submenuWidth();
  });
  /**/

});